package main


import (
	"log"
        "fmt"
	"io"	
	"os/exec"
	"runtime"
	"net/http"
	"encoding/json"
	"code.google.com/p/plotinum/vg"
	"code.google.com/p/plotinum/vg/vgimg"
	"code.google.com/p/plotinum/plotter"
	"code.google.com/p/plotinum/plot"
	"image/color"
	"encoding/base64"
	"bytes"
)

const IP="127.0.0.1"
const PORT="8080"

type Response map[string]interface{}

func (r Response) String() (s string) {
        b, err := json.Marshal(r)
        if err != nil {
                s = ""
                return
        }
        s = string(b)
        return
}

type test_struct struct {
    Test string
}


func myHandler(rw http.ResponseWriter, req *http.Request) {
         req.ParseForm()
    var t test_struct
	
    for key, _ := range req.Form {
        log.Println(key)
        //LOG: {"test": "that"}
        err := json.Unmarshal([]byte(key), &t)
        if err != nil {
            log.Println(err.Error())
        }
    }
	rw.Header().Set("Access-Control-Allow-Origin", "*" )
	rw.Header().Set("Access-Control-Allow-Headers", "Origin, X-Requested-With, X-Prototype-Version")
        rw.Header().Set("Access-Control-Allow-Methods", "POST" )

	fmt.Fprint(rw, Response{"success": true, "image":plot01()})
    return
}

func plot01() string {
	pts := make(plotter.XYs, 2)
        pts[0].X=14
        pts[0].Y=10
        pts[1].X=10.2
        pts[1].Y=0.5 

        sts := make(plotter.XYs, 2)
	sts[0].X=4
	sts[0].Y=9
	sts[1].X=1.2
        sts[1].Y=5	
	scatterData := sts

	st2s := make(plotter.XYs, 2)
        st2s[0].X=10
        st2s[0].Y=7
        st2s[1].X=9
        st2s[1].Y=2      
        scatter2Data := st2s

        p, err := plot.New()
        if err != nil {
                panic(err)
	}

        p.Title.Text = "Perceptron's weights - generation 0"
        p.X.Label.Text = "X"
        p.Y.Label.Text = "Y"
        
	p.Add(plotter.NewGrid())
        
        s, err := plotter.NewScatter(scatterData)
        if err != nil {
                panic(err)
        }
        s.GlyphStyle.Color = color.RGBA{R:0 , G:255,  B:0 , A: 255}
	
	s2, err := plotter.NewScatter(scatter2Data)
        if err != nil {
                panic(err)
        }
        s2.GlyphStyle.Color = color.RGBA{R:255 , G:0,  B:0 , A: 255}
	
        lpLine, lpPoints, err := plotter.NewLinePoints(pts)
        if err != nil {
                panic(err)
        }
        lpLine.Color = color.RGBA{G: 255, A: 255}
        lpPoints.Shape = plot.PyramidGlyph{}
        lpPoints.Color = color.RGBA{R: 255, A: 255}

	p.Add(s, s2, lpLine,lpPoints)
        p.Legend.Add("Good", s)
	p.Legend.Add("Bad", s2)
        p.Legend.Add("Weights", lpLine )

	w, h := vg.Inches(4), vg.Inches(4)
        var c interface {
                vg.Canvas
                Size() (w, h vg.Length)
                io.WriterTo
        }
        c = vgimg.PngCanvas{Canvas: vgimg.New(w, h)}
        
	p.Draw(plot.MakeDrawArea(c))
	var buf=new(bytes.Buffer)
	_, err = c.WriteTo(buf)
        return base64.StdEncoding.EncodeToString(buf.Bytes())
}

func startBrowser() bool {
        // try to start the browser
        var args []string
        switch runtime.GOOS {
        case "darwin":
                args = []string{"open"}
        case "windows":
                args = []string{"cmd", "/c", "start"}
        default:
                args = []string{"xdg-open"}
        }
        cmd := exec.Command(args[0], append(args[1:], "http://nn.cisary.com/?ip="+IP+"&port="+PORT)...)
        return cmd.Start() == nil
}

func pingHandler(rw http.ResponseWriter, req *http.Request) {
	rw.Header().Set("Content-Type", "text/html"  )
	rw.Header().Set("Access-Control-Allow-Origin", "*" )
	rw.Header().Set("Access-Control-Allow-Headers", "Origin, X-Requested-With, X-Prototype-Version" )
	rw.Header().Set("Access-Control-Allow-Methods", "POST" )
	fmt.Fprint(rw, "Connected to "+ IP+ ":"+ PORT)
	return
}

func main() {
    	http.HandleFunc("/nn",myHandler)
	http.HandleFunc("/ping",pingHandler)
	startBrowser() 
    	log.Fatal(http.ListenAndServe(":"+PORT, nil))
}
